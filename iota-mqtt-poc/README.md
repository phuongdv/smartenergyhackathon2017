# iota-mqtt-poc

IOTA Proof of Concept, service that stores MQTT messages on the tangle, processing payment.

## How to use

Run iota-mqtt-poc with IDE or Maven


```
$ mvn clean package -DskipTests
$ mvn spring-boot:run -Drun.jvmArguments="-Dspring.profiles.active=stag" 
```