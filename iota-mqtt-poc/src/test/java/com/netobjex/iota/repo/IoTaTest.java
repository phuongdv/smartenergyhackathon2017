package com.netobjex.iota.repo;

import jota.IotaAPI;
import jota.dto.response.SendTransferResponse;
import jota.error.*;
import jota.model.Transaction;
import jota.model.Transfer;
import jota.utils.TrytesConverter;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class IoTaTest {

    String TEST_SEED1 = "N9XXNYHHOMGKEVYTPXKVREKCXMIDKIVMHUZVBMNWRHLMENBOBTTETELGFSDWARPRJIGPQ9ZNEXPF9GGEC";
    String TEST_ADRESS1 = "BAUYTMCSFHOSHNZVLQXSZCYMFMURCOSFSHXWSUTCB9GEBDDAL9FDLJSQDWDKYJJWBLXZBIWWFMMXRZQOW";
    String TEST_ADRESS_RECIEVED = "CZGCSDOEZHOBFF9NCLRRRAMGRS9RPZAMXYQYEWGIGENUPRNXLPFQAAIGKYHIOVXDXSVBG9BCSCLPWFKMZ";

    @Test
    public void testNodeInfo() throws NoInclusionStatesException, InvalidTrytesException, NoNodeInfoException, ArgumentException, InvalidBundleException, InvalidSecurityLevelException, InvalidAddressException, InvalidSignatureException {

        IotaAPI api = new IotaAPI.Builder()
                .protocol("https")
                .host("testnet.tangle.works")
                .port("443")
                .build();


//        GetNodeInfoResponse response = api.getNodeInfo();
//        System.out.println(response);

//        GetAccountDataResponse gad = api.getAccountData(TEST_SEED1, 2, 0, true, 0, true, 0, 0, true, 0);

//        Bundle[] bundles = gad.getTransfers();
//        for (Bundle bundle : bundles) {
//            System.out.println(bundle.getTransactions());
//        }


//        List<Transaction> ftr = api.findTransactionObjects(new String[]{TEST_ADRESS_RECIEVED});
//        ftr.forEach(transaction -> {
//            System.out.println(transaction.getSignatureFragments());
//            try {
//                String dataEncrypt = StringUtils.stripEnd(transaction.getSignatureFragments(), "9");
//                System.out.println(TrytesConverter.toString(dataEncrypt));
////            System.out.println(transaction.getSignatureFragments());
//
//            }catch (Exception ex){
//
//            }
//
//        });


        String json = "{\"hello\":9}";
        List<Transfer> transfers = new ArrayList<>();
        transfers.add(new jota.model.Transfer("CZGCSDOEZHOBFF9NCLRRRAMGRS9RPZAMXYQYEWGIGENUPRNXLPFQAAIGKYHIOVXDXSVBG9BCSCLPWFKMZ", 100, TrytesConverter.toTrytes(json), "HACKATHONHACKATHONHACKATHON"));
        transfers.forEach(tr -> {
            tr.getHash();
        });

        try {
            SendTransferResponse str = api.sendTransfer(TEST_SEED1, 2, 5, 9, transfers, null, null);
        } catch (NotEnoughBalanceException e) {
            e.printStackTrace();
        } catch (InvalidTransferException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void ParseData() {

        String a = "{\"hello\":9}9999999";

        System.out.println(StringUtils.stripEnd(a, "9"));


    }
}
