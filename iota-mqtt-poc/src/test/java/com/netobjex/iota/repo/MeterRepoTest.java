package com.netobjex.iota.repo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netobjex.iota.config.Cache;
import com.netobjex.iota.config.CurrencyConfig;
import com.netobjex.iota.model.MeterSN;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class MeterRepoTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MeterRepo meterRepo;


    @Test
    public void testFindMeters() {

        System.out.println("####setup meters...");
        meterRepo.save(new MeterSN(new Date(System.currentTimeMillis() + (1000 * 1)), 1, 1, 1));
        meterRepo.save(new MeterSN(new Date(System.currentTimeMillis() + (1000 * 10)), 2, 2, 2));
        meterRepo.save(new MeterSN(new Date(System.currentTimeMillis() + (1000 * 20)), 3, 3, 3));
        meterRepo.save(new MeterSN(new Date(System.currentTimeMillis() + (1000 * 30)), 4, 4, 4));
        meterRepo.save(new MeterSN(new Date(System.currentTimeMillis() + (1000 * 40)), 5, 5, 5));

        List<MeterSN> meterSNList = meterRepo.findAllByTimestampAfter(new Date(System.currentTimeMillis() + (1000 * 20)));

        meterSNList.sort(new Comparator<MeterSN>() {
            @Override
            public int compare(MeterSN o1, MeterSN o2) {
                return (int) (o2.getTimestamp().getTime() - o1.getTimestamp().getTime());
            }
        });
        meterSNList.forEach(s -> System.out.println(s));


    }



}