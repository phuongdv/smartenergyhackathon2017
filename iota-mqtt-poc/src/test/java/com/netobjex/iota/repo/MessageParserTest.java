package com.netobjex.iota.repo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netobjex.iota.config.Cache;
import com.netobjex.iota.config.CurrencyConfig;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MessageParserTest {

    @Test
    public void testParseMessage() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        String msg = "{\"phaseA\":{\"Status\":\"HH\",\"ActiveDemand\":240.448952,\"Temperature\":33.35207,\"ReactiveEnergyI\":0.0162,\"ReactivePower\":-0.948111,\"ActiveEnergyR\":0.0,\"ReactiveDemand\":240.448952,\"ReactiveEnergyC\":2.0E-4,\"ActivePower\":240.072479,\"Voltage\":239.975372,\"ActiveEnergyF\":4.131243,\"Frequency\":49.987324,\"Current\":1.000104},\"phaseB\":{\"Status\":\"HH\",\"ActiveDemand\":240.439892,\"Temperature\":34.703655,\"ReactiveEnergyI\":0.0162,\"ReactivePower\":-0.929833,\"ActiveEnergyR\":0.0,\"ReactiveDemand\":240.439892,\"ReactiveEnergyC\":2.0E-4,\"ActivePower\":240.064133,\"Voltage\":239.980759,\"ActiveEnergyF\":4.131143,\"Frequency\":49.999722,\"Current\":1.000043},\"payloadType\":\"SmartMeter\",\"Time\":\"2017-09-09 03:40:00\",\"deviceId\":\"GDVJ0TNNMTM1RLQL\",\"phaseC\":{\"Status\":\"HH\",\"ActiveDemand\":240.439892,\"Temperature\":36.633373,\"ReactiveEnergyI\":0.0131,\"ReactivePower\":-0.723203,\"ActiveEnergyR\":0.0,\"ReactiveDemand\":240.439892,\"ReactiveEnergyC\":2.0E-4,\"ActivePower\":240.025192,\"Voltage\":239.9758,\"ActiveEnergyF\":4.131042,\"Frequency\":49.999722,\"Current\":0.999906},\"MeterSN\":1063}";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Map<String, Object> mapReceived = new HashMap<>();
        mapReceived = objectMapper.readValue(msg, HashMap.class);

        Date objDate = df.parse(mapReceived.get("Time").toString());

        Map<String, Object> mapPhaseA = (Map<String, Object>) mapReceived.get("phaseA");
        Map<String, Object> mapPhaseB = (Map<String, Object>) mapReceived.get("phaseA");
        Map<String, Object> mapPhaseC = (Map<String, Object>) mapReceived.get("phaseA");

        double activeEnergyFA = Double.parseDouble(mapPhaseA.get("ActiveEnergyF").toString());
        double activeEnergyFB = Double.parseDouble(mapPhaseB.get("ActiveEnergyF").toString());
        double activeEnergyFC = Double.parseDouble(mapPhaseC.get("ActiveEnergyF").toString());

        //        MeterSN meterSN = new MeterSN();
        Cache.accumActiveEnergyF += (activeEnergyFA + activeEnergyFB + activeEnergyFC);

        Map<String, Object> mapResponse = new HashMap<>();
        mapResponse.put("timestamp", new Date().getTime());
        double currentSpendUSD = (Cache.accumActiveEnergyF * CurrencyConfig.USD_PER_KWH);
        double currentSpendIOTA = currentSpendUSD / CurrencyConfig.EXCH_RATE_USD_TO_IOTA;
        mapResponse.put("usd", currentSpendUSD);
        mapResponse.put("iota", currentSpendIOTA);

        System.out.println(objectMapper.writeValueAsString(mapResponse));

    }
}
