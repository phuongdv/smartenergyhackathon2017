package com.netobjex.iota.repo;

import com.netobjex.iota.model.MeterSN;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface MeterRepo extends CrudRepository<MeterSN, Long> {
    //    @Query("SELECT e FROM UserSession e WHERE e.expiration < CURRENT_TIMESTAMP")
    List<MeterSN> findAllByTimestampAfter(Date timestamp);
}
