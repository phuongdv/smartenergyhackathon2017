package com.netobjex.iota.model;


import javax.persistence.*;
import java.util.Date;

@Entity
public class MeterSN {


    double activeEnergyFA;
    double activeEnergyFB;
    double activeEnergyFC;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;


    public MeterSN(Date timestamp, double activeEnergyFA, double activeEnergyFB, double activeEnergyFC) {
        this.timestamp = timestamp;
        this.activeEnergyFA = activeEnergyFA;
        this.activeEnergyFB = activeEnergyFB;
        this.activeEnergyFC = activeEnergyFC;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public double getActiveEnergyFA() {
        return activeEnergyFA;
    }

    public void setActiveEnergyFA(double activeEnergyFA) {
        this.activeEnergyFA = activeEnergyFA;
    }

    public double getActiveEnergyFB() {
        return activeEnergyFB;
    }

    public void setActiveEnergyFB(double activeEnergyFB) {
        this.activeEnergyFB = activeEnergyFB;
    }

    public double getActiveEnergyFC() {
        return activeEnergyFC;
    }

    public void setActiveEnergyFC(double activeEnergyFC) {
        this.activeEnergyFC = activeEnergyFC;
    }


    public double sumThreePhaseEnergy() {

        return this.activeEnergyFA + this.activeEnergyFB + this.activeEnergyFC;

    }

    @Override
    public String toString() {
        return "MeterSN{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", activeEnergyFA=" + activeEnergyFA +
                ", activeEnergyFB=" + activeEnergyFB +
                ", activeEnergyFC=" + activeEnergyFC +
                '}';
    }
}
