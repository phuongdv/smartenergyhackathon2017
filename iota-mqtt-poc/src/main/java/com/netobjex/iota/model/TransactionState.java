package com.netobjex.iota.model;

public class TransactionState {

    boolean confirmed;
    long timestamp;
    String hash;
    String address;
    long value;
    String tag;

    Object kwh;
    Object usd;
    Object iota;

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }


    public void setIota(String iota) {
        this.iota = iota;
    }

    public Object getKwh() {
        return kwh;
    }

    public void setKwh(Object kwh) {
        this.kwh = kwh;
    }

    public Object getUsd() {
        return usd;
    }

    public void setUsd(Object usd) {
        this.usd = usd;
    }

    public Object getIota() {
        return iota;
    }

    public void setIota(Object iota) {
        this.iota = iota;
    }
}
