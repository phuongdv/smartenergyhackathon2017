package com.netobjex.iota.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netobjex.iota.config.Cache;
import com.netobjex.iota.config.CurrencyConfig;
import com.netobjex.iota.service.IotaService;
import jota.dto.response.GetAccountDataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/smartenergy/api")
public class SmartController {

    private static final Logger LOG = LoggerFactory.getLogger(SmartController.class);
    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    IotaService iotaService;


    @RequestMapping(value = "/debug", method = RequestMethod.GET)
    public ResponseEntity<String> debug() throws JsonProcessingException {
        Map<String, Object> mapper = new HashMap<>();
//        mapper.put("lastCals", Cache.lastCals);
        mapper.put("accumActiveEnergyF", Cache.accumActiveEnergyF);
        double currentSpendUSD = (Cache.accumActiveEnergyF * CurrencyConfig.USD_PER_KWH);
        mapper.put("usd", currentSpendUSD);
        mapper.put("iota", (long) (currentSpendUSD / CurrencyConfig.EXCH_RATE_USD_TO_IOTA));
        return new ResponseEntity<String>(objectMapper.writeValueAsString(mapper), HttpStatus.OK);
    }


    @RequestMapping(value = "/sender/wallet", method = RequestMethod.GET)
    public ResponseEntity<String> consumerWallet() throws Exception {
        Map<String, Object> mapper = new HashMap<>();
        GetAccountDataResponse account = iotaService.getAccountData();
        long iotaBlance = account.getBalance();
        double usdBlance = iotaBlance * CurrencyConfig.EXCH_RATE_USD_TO_IOTA;
//        mapper.put("")
        mapper.put("balanceIOTA", iotaBlance);
        mapper.put("balanceUSD", usdBlance);
        return new ResponseEntity<String>(objectMapper.writeValueAsString(mapper), HttpStatus.OK);
    }

    @RequestMapping(value = "/sender/debetAmount", method = RequestMethod.GET)
    public ResponseEntity<String> debet() throws Exception {

        Map<String, Object> mapResponse = new HashMap<>();
        mapResponse.put("timestamp", new Date().getTime());
        double currentSpendUSD = (Cache.accumActiveEnergyF * CurrencyConfig.USD_PER_KWH);
        long currentSpendIOTA = (long) (currentSpendUSD / CurrencyConfig.EXCH_RATE_USD_TO_IOTA);
        mapResponse.put("usd", currentSpendUSD);
        mapResponse.put("iota", currentSpendIOTA);
        return new ResponseEntity<String>(objectMapper.writeValueAsString(mapResponse), HttpStatus.OK);
    }


    @RequestMapping(value = "/sender/transactions", method = RequestMethod.GET)
    public ResponseEntity<String> getTransactions() throws Exception {
//        Map<String, Object> mapper = new HashMap<>();
        return new ResponseEntity<String>(objectMapper.writeValueAsString(iotaService.findTransactionObjects()), HttpStatus.OK);
    }


    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public ResponseEntity<String> payment() throws JsonProcessingException {
        Map<String, Object> mapper = new HashMap<>();


        double currentSpendUSD = (Cache.accumActiveEnergyF * CurrencyConfig.USD_PER_KWH);
        long currentSpendIOTA = (long) (currentSpendUSD / CurrencyConfig.EXCH_RATE_USD_TO_IOTA);

        if (currentSpendIOTA > 0) {
            try {

                Map<String, Object> mapObj = new HashMap<>();
                mapObj.put("kwh", Cache.accumActiveEnergyF);
                mapObj.put("usd", currentSpendUSD);
                mapObj.put("iota", currentSpendIOTA);
                iotaService.sendTranfers(currentSpendIOTA, objectMapper.writeValueAsString(mapObj));
                Cache.accumActiveEnergyF = 0;
                mapper.put("status", "Ok");
                return new ResponseEntity<String>(objectMapper.writeValueAsString(mapper), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mapper.put("status", "Error");
        return new ResponseEntity<String>(objectMapper.writeValueAsString(mapper), HttpStatus.BAD_REQUEST);

    }


}
