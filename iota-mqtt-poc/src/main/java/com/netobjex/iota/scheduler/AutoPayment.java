package com.netobjex.iota.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netobjex.iota.config.BrokerTopic;
import com.netobjex.iota.controller.SmartController;
import com.netobjex.iota.message.MqttPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class AutoPayment {
    @Autowired
    MqttPublisher mqttPublisher;

    @Autowired
    SmartController smartController;

    ObjectMapper objectMapper = new ObjectMapper();

    @Scheduled(initialDelay = 30 * 60 * 1000, fixedRate = 30 * 60 * 1000)
    public void autoPayment() {

        try {
            smartController.payment();
            Map<String, Object> mapResponse = new HashMap<>();
            mapResponse.put("status", "update");
            mqttPublisher.publish(BrokerTopic.NOTIFY_UPDATE, objectMapper.writeValueAsString(mapResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


}
