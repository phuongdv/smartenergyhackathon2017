package com.netobjex.iota.config;

public class CurrencyConfig {

    public static double EXCH_RATE_USD_TO_IOTA = 0.6;
    public static double USD_PER_KWH = 0.12;
}
