package com.netobjex.iota.config;

public class BrokerTopic {
    public static final String TOPIC_IOTA_INPUT_GMAIL = "/topic/device/{queue}/in";
    public static final String NOTIFY_UPDATE = "/notify/smartmeter/update";

}
