package com.netobjex.iota.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.netobjex.iota.config.EnvConfig;
import com.netobjex.iota.model.TransactionState;
import jota.IotaAPI;
import jota.dto.response.GetAccountDataResponse;
import jota.dto.response.GetInclusionStateResponse;
import jota.dto.response.SendTransferResponse;
import jota.model.Transaction;
import jota.model.Transfer;
import jota.utils.TrytesConverter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IotaService {


    private static final String TEST_TAG = "HACKATHONHACKATHONHACKATHON";
    private static int MWM = 9;
    IotaAPI iotaClient;

    ObjectMapper objectMapper = new ObjectMapper();

    @PostConstruct
    public void init() {
        iotaClient = new IotaAPI.Builder()
                .protocol("https")
                .host("testnet.tangle.works")
                .port("443")
                .build();
    }

    public GetAccountDataResponse getAccountData() throws Exception {
        GetAccountDataResponse gad = iotaClient.getAccountData(EnvConfig.getEnv().getProperty("wallet.sender.seed"), 2, 0, true, 0, false, 0, 0, false, 0);
        return gad;
    }

    public List<TransactionState> findTransactionObjects() throws Exception {
        List<Transaction> ftr = iotaClient.findTransactionObjects(new String[]{EnvConfig.getEnv().getProperty("wallet.receiver.address")});
        //
        List<String> tranHashList = new ArrayList<>();
        for (Transaction transaction : ftr) {
            tranHashList.add(transaction.getHash());
        }

        GetInclusionStateResponse res = iotaClient.getLatestInclusion(tranHashList.toArray(new String[tranHashList.size()]));

        boolean[] confirmed = res.getStates();

        List<TransactionState> stateList = new ArrayList<>();

        for (int i = 0; i < confirmed.length; i++) {
            Transaction transaction = ftr.get(i);
            TransactionState transactionState = new TransactionState();
            transactionState.setAddress(transaction.getAddress());
            transactionState.setConfirmed(confirmed[i]);
            transactionState.setHash(transaction.getHash());
            transactionState.setValue(transaction.getValue());
            transactionState.setTimestamp(transaction.getTimestamp());
            transactionState.setTag(transaction.getTag());
            try {

                String dataEncrypt = StringUtils.stripEnd(transaction.getSignatureFragments(), "9");
                String json = TrytesConverter.toString(dataEncrypt);
                Map<String, Object> mapObj = objectMapper.readValue(json, HashMap.class);
                transactionState.setKwh(mapObj.get("kwh"));
                transactionState.setUsd(mapObj.get("usd"));
                transactionState.setIota(mapObj.get("iota"));

            } catch (Exception ex) {


            }

            stateList.add(transactionState);
        }

        return stateList;
    }

    public SendTransferResponse sendTranfers(long value, String message) throws Exception {
        List<Transfer> transfers = new ArrayList<>();
        transfers.add(new jota.model.Transfer(EnvConfig.getEnv().getProperty("wallet.receiver.address"), value, TrytesConverter.toTrytes(message), TEST_TAG));
        SendTransferResponse str = iotaClient.sendTransfer(EnvConfig.getEnv().getProperty("wallet.sender.seed"), 2, 5, MWM, transfers, null, null);
        return str;
    }


}
