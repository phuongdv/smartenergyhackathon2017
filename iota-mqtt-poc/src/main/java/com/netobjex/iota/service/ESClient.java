package com.netobjex.iota.service;


import com.netobjex.iota.config.EnvConfig;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author phuongdv
 * This class is for ElasticSearch Client API
 */
//@Component
public class ESClient {

    private static final Logger LOG = LoggerFactory.getLogger(ESClient.class);
    RestClient esRestClient = null;

    public ESClient() {

        try {
            String esHost = EnvConfig.getEnv().getProperty("es.host");
            String esPort = EnvConfig.getEnv().getProperty("es.port");
            String esScheme = EnvConfig.getEnv().getProperty("es.scheme");
            LOG.info("init rest client to {}:{}", esHost, esPort);
            esRestClient = RestClient.builder(
                    new HttpHost(esHost, Integer.parseInt(esPort), esScheme))
                    .build();

        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error("Error: Can't create Rest Client, no ES cluster, err : {}", ex.getMessage());
        }

    }

    public RestClient getEsRestClient() {
        return esRestClient;
    }
}
