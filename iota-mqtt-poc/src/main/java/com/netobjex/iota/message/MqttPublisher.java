package com.netobjex.iota.message;


import com.netobjex.iota.config.EnvConfig;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by phuongdv on 08/09/2017.
 * <p>
 * Utility class for connecting and maintaining connectivity to MQTT
 * message brokers. It also provides a simple method to publish MQTT
 * message payloads to an MQTT Server.
 * * @Autowire should be used but I prefer my Singleton pattern to @Autowire Singleton :D
 */

@Service
public class MqttPublisher implements MqttCallbackExtended {
    private static final Logger LOG = LoggerFactory.getLogger(MqttPublisher.class);

    static AtomicLong counter = new AtomicLong(1);
    static String uptimeTime = new Date().toString();
    MqttAsyncClient mqttClient = null;
    String brokerUserName = "";
    String brokerPassword = "";
    String brokerHost = "";
    MqttConnectOptions mqttConnectOptions;


    @PostConstruct
    public void init() {
        LOG.info("starting MQTT PUBLISHER CLIENT....");
        brokerHost = "tcp://" + EnvConfig.getEnv().getProperty("broker.host") + ":" + EnvConfig.getEnv().getProperty("broker.mqtt.port");
        this.brokerPassword = EnvConfig.getEnv().getProperty("broker.password");
        this.brokerUserName = EnvConfig.getEnv().getProperty("broker.username");
        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName(brokerUserName);
        mqttConnectOptions.setPassword(brokerPassword.toCharArray());
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setKeepAliveInterval(20);
        mqttConnectOptions.setAutomaticReconnect(true);

        // Connecting
        connect();
    }

    @Override
    public void connectionLost(Throwable cause) {
        LOG.error("MqttPublisher-Connection lost!");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    @Override
    public void connectComplete(boolean bReconnect, String host) {
        LOG.info("MqttPublisher- Connected to " + host + " Auto reconnect ? " + bReconnect);

        try {
            if (!bReconnect) {
                mqttClient.connect(mqttConnectOptions);
            }

        } catch (Exception ex) {
//            ex.printStackTrace();
        }
    }

    private void connect() {
//        long val = counter.incrementAndGet();
        if (mqttClient == null) {
            MemoryPersistence persistence = new MemoryPersistence();
            try {
                mqttClient = new MqttAsyncClient(brokerHost, "@pub-" + System.currentTimeMillis(), persistence);
                mqttClient.setCallback(this);
                mqttClient.connect(mqttConnectOptions);
                LOG.info("Successfully connected to MQTT Broker! [" + brokerHost + "]");
            } catch (Exception e) {
                LOG.error("Error connecting to MQTT Broker", e);
                connectionLost(e);
            }
        }

    }

    /************************
     * Generic method to publish MQTT messages to an MQTT broker, it removed the
     * complexity to connect, manage and maintain persistent connectivity with
     * the broker. It als handles re-connects in the event the broker goes down
     * or some other network event caused the connection to be disconnected.
     */

    public void publish(String topic, String message) {
        LOG.debug("Publishing MQTT message to topic " + topic + System.lineSeparator()
                + "Messages Published since uptime [" + uptimeTime + "] is " + counter.get());

        if (mqttClient == null) {
            LOG.error("Unable to publish MQTT Message, MQTT Client is null or cannot coonnect to broker");
            return;
        }
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(2);
        mqttMessage.setRetained(false);
        mqttMessage.setPayload(message.getBytes());
        try {
            mqttClient.publish(topic, mqttMessage);
        } catch (MqttException e) {
            LOG.error("Error publishing MQTT message", e);
        }
    }


}