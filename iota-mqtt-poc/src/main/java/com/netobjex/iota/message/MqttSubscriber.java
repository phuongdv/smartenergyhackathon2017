package com.netobjex.iota.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netobjex.iota.config.BrokerTopic;
import com.netobjex.iota.config.Cache;
import com.netobjex.iota.config.EnvConfig;
import com.netobjex.iota.utils.QueryParamMapper;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by phuongdv on 08/09/2017.
 * This class for connecting and maintaining connectivity to MQTT
 * message brokers
 */
@Service
public class MqttSubscriber implements MqttCallbackExtended {
    final static ObjectMapper objectMapper = new ObjectMapper();
    static final Logger LOG = LoggerFactory.getLogger(MqttSubscriber.class);
    static AtomicLong counterSub = new AtomicLong(1);
    static String brokerUserName = "";
    static String brokerPassword = "";
    static String brokerHost = "";
    static String queueName = "";
    //
    @Autowired
    MqttPublisher mqttPublisher;
    private MqttAsyncClient mqttClient;

    @PostConstruct
    public void init() {
        LOG.info("Init MQTT SUBSCRIBER topic");
        brokerHost = "tcp://" + EnvConfig.getEnv().getProperty("broker.host") + ":" + EnvConfig.getEnv().getProperty("broker.mqtt.port");
        this.brokerPassword = EnvConfig.getEnv().getProperty("broker.password");
        this.brokerUserName = EnvConfig.getEnv().getProperty("broker.username");
        LOG.info("Starting MQTT Message Listener....");
        Map<String, String> map = new HashMap<String, String>();
        map.put("queue", EnvConfig.getEnv().getProperty("ap.queuename"));
        queueName = QueryParamMapper.createQuery(BrokerTopic.TOPIC_IOTA_INPUT_GMAIL, map);
        subscribe();
    }

    @Override
    public void connectComplete(boolean bReconnect, String host) {
        final String METHOD = "connectComplete";
        LOG.info(METHOD + " Connected to " + host + " Auto reconnect ? " + bReconnect);
        try {
            if (!bReconnect) {
                mqttClient.subscribe(queueName, 2);
            }
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable e) {
        LOG.error("MqttSubscriber-Connection lost!");
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0) {
    }

    @Override
    public void messageArrived(String topic, MqttMessage msg) throws Exception {

        try {
            LOG.info("Got => message ON TOPIC " + topic);
            processingMessage(msg.toString(), topic);
//            System.out.println(msg);
        } catch (Exception ex) {

        }
    }

    private void processingMessage(String msg, String topic) throws IOException, ParseException {
        //2017-09-08 21:50:00
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Map<String, Object> mapReceived = new HashMap<>();
        mapReceived = objectMapper.readValue(msg, HashMap.class);

        Date objDate = df.parse(mapReceived.get("Time").toString());

        Map<String, Object> mapPhaseA = (Map<String, Object>) mapReceived.get("phaseA");
        Map<String, Object> mapPhaseB = (Map<String, Object>) mapReceived.get("phaseA");
        Map<String, Object> mapPhaseC = (Map<String, Object>) mapReceived.get("phaseA");

        double activeEnergyFA = Double.parseDouble(mapPhaseA.get("ActiveEnergyF").toString());
        double activeEnergyFB = Double.parseDouble(mapPhaseB.get("ActiveEnergyF").toString());
        double activeEnergyFC = Double.parseDouble(mapPhaseC.get("ActiveEnergyF").toString());

        //        MeterSN meterSN = new MeterSN();
        Cache.accumActiveEnergyF += (activeEnergyFA + activeEnergyFB + activeEnergyFC);

        Map<String, Object> mapResponse = new HashMap<>();
        mapResponse.put("status", "update");
        mqttPublisher.publish(BrokerTopic.NOTIFY_UPDATE, objectMapper.writeValueAsString(mapResponse));


    }

    /**
     * Generic method to subscribe for MQTT messages published on a topic on an
     * MQTT broker, it removed the complexity to connect, manage and maintain
     * persistent connectivity with the broker. It also handles re-connects in
     * the event the borker goes down or some other network event caused the
     * connection to be disconnected.
     */

    public void subscribe() {
        try {

            if (mqttClient == null) {
                long val = counterSub.incrementAndGet();
                MemoryPersistence persistence = new MemoryPersistence();
                LOG.info("Connecting to broker @{} and subscribed to topic {}", brokerHost, queueName);
                mqttClient = new MqttAsyncClient(brokerHost, "@sub-" + System.currentTimeMillis(), persistence);
                MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
                mqttConnectOptions.setUserName(this.brokerUserName);
                mqttConnectOptions.setPassword(this.brokerPassword.toCharArray());
                mqttConnectOptions.setCleanSession(true);
                mqttConnectOptions.setKeepAliveInterval(20);
                mqttConnectOptions.setAutomaticReconnect(true);
                mqttClient.setCallback(this);
                mqttClient.connect(mqttConnectOptions);
                // You can also subscribe on the came connection using wildcard
//                System.out.println(queueName);
                mqttClient.subscribe(queueName, 2);
                LOG.info("Successfully connected to broker @{} and subscribed to topic {}", brokerHost, queueName);
            }
        } catch (Exception e) {
//            e.printStackTrace();
            LOG.error("Error connecting to MQTT SERVER {}", e.getMessage());
            connectionLost(e);
        }

    }


}
