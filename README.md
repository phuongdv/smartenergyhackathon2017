# Smart Electricity Metering

## Problem

A negawatt is a negative megawatt: a megawatt of power saved by increasing efficiency or reducing consumption and efficient use of energy is a no brainer when aiming to tackle climate change. Create an innovative solution that can help residential, commercial and industrial sectors use their energy more efficiently. It can be either product or service leveraging digital technology and big data analytics. Put the user at the centre and create an awesome user experience, together with a simple way to deploy the solution.

## Solution

A method of capturing electricity usage metrics using a Smart Meter and then paying for power through a cryptocurrency with low/no transaction fees

# Solution Benefits

* Low cost edge device
* Low transaction fees
* Verifiable,Secure, immutable recordkeeping
* IoT Data Acquisition
* Real-Time Analytics

## Built With

IOTA, Netbojex IOT Platform IOS
