//
//  AppDelegate.swift
//  smartmeter
//
//  Created by Mac on 9/8/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit
import Async

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if UserManager.shared.loggedIn {
            if let controller = UIStoryboard.mainViewController() {
                self.window?.rootViewController = controller
                self.window?.makeKeyAndVisible()
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.checkToken()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - Public func
    
    func login() {
        
        DispatchQueue.main.async(execute: {
            if let controller = UIStoryboard.rootViewController() {
                self.window?.rootViewController = controller
                self.window?.makeKeyAndVisible()
                self.window?.rootViewController?.view!.alpha = 0
                UIView.animate(withDuration: 1.0, animations: { () -> Void in
                    self.window!.rootViewController!.view.alpha = 1.0
                })
            }
        })
    }
    
    func home() {
        
        DispatchQueue.main.async(execute: {
            if let controller = UIStoryboard.mainViewController() {
                self.window?.rootViewController = controller
                self.window?.makeKeyAndVisible()
                self.window?.rootViewController?.view!.alpha = 0
                UIView.animate(withDuration: 1.0, animations: { () -> Void in
                    self.window!.rootViewController!.view.alpha = 1.0
                })
            }
        })
    }
    
    // MARK: Private
    
    private func checkToken() {
        
        if let loginDate = UserDefaults.standard.value(forKey: LOGIN_TIME_KEY) as? Date, UserManager.shared.loggedIn {
            let date = Date()
            let seconds = date.timeIntervalSinceNow - loginDate.timeIntervalSinceNow
            let ttl = Double(UserManager.shared.user?.ttl ?? 0)
            if seconds > ttl {
                Async.main(after: 1.0) { () -> Void in
                    Utils.showAlertView(SESSION_EXPIRED, cancelHandler: { (alert) in
                        UserManager.logout()
                    })
                }
            } else {
                UserManager.shared.checkToken({
                    //Success
                }, failure: { (error) in
                    print("Token expried: \(error)")
                    Async.main(after: 0.0) { () -> Void in
                        Utils.showAlertView(SESSION_EXPIRED, cancelHandler: { (alert) in
                            UserManager.logout()
                        })
                    }
                })
            }
        }
    }
}

