//
//  AppConfig.swift
//  Netobjex
//
//  Created by Mac Lich on 6/13/17.
//  Copyright © 2017 maclich. All rights reserved.
//

import Foundation

let appName = "Smart Electricity Metering"
let ERROR_DEFAULT = "An error occurred while processing, please try again later"
let SESSION_EXPIRED = "Your session has expired. Please re-login to renew your session."
let PAYMENT_SUCCESS = "Thank you! Your payment was processed successfully."
let PAYMENT_HISTORY_EMPTY = "Your payment history is empty"
let LOGOUT_CONFIRM_MESSAGE = "Do you wish to log out?"
let PAYMENT_CONFIRM_MESSAGE = "Do you wish to pay now?"

let X_APP_BUNDLE_ID = "com.netobjex.smartemetering"
let X_API_AUTH_KEY     = "vanqCwLaXgy9VWEMdPqoAHMXZDMBkAEo"

let BASE_URL_STRING  = "https://api2.netobjex.com"
let BASE_URL_STRING_FOR_SMART_METER  = "https://ms.dev.netobjex.com:9115"

let MQTT_HOST  = "mq-prod.netobjex.com"
let MQTT_PORT: UInt16  = 1883
let MQTT_Username = "admin"
let MATT_Password = "ayVsamhfGZSJvN9"

extension Notification.Name {
    static let powerChanged = Notification.Name("power_changed_notification")
}

let Cent = "\u{00A2}"
let APIDateFormat = "yyyy-MM-dd HH:mm:ss"
