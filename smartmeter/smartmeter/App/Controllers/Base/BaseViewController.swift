//
//  BaseViewController.swift
//  clinitraq
//
//  Created by Mac Lich on 12/21/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UI
    
    func initUI() {
        
        let color = ColorService.blackColor
        self.navigationController?.navigationBar.tintColor = color
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: color, NSFontAttributeName: FontService.robotoRegular(19)]
        self.view.backgroundColor = ColorService.backgroundColor
    }
    
    func addLogoutButtonBarItem() {
        let image = UIImage(named: "ic_logout")
        let settingsBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.clickedOnRightBarButtonItem))
        self.navigationItem.rightBarButtonItem = settingsBarButtonItem
    }
    
    // MARK: Actions
    
    func clickedOnRightBarButtonItem() {
        
    }
}
