//
//  LoginViewController.swift
//  smartmeter
//
//  Created by Mac on 9/8/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var loginButton: LoadingButton!
    
    @IBOutlet weak var textFieldHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var logoWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: UI
    
    func setupUI() {
        
        if UIScreen.main.bounds.width <= 320 {
            self.mainHeightConstraint.constant = 245
            self.textFieldHeightConstraint.constant = 30
            self.logoWidthConstraint.constant = 180
            self.titleLabel.font = FontService.robotoMedium(17)
            let font = FontService.robotoLight(13)
            self.mailTextField.font = font
            self.passwordTextField.font = font
            self.loginButton.titleLabel?.font = FontService.robotoRegular(15)
        }
        
        self.contentView.setCornerRadius(radius: 4)
        self.containerView.addShadow(4, opacity: 0.1, size: CGSize(width: 3, height: 3))
        self.loginButton.setCornerRadius(radius: 4)
        self.loginButton.addCompletionHanlder { (state) in
            self.mailTextField.isEnabled = state != .start
            self.passwordTextField.isEnabled = state != .start
        }
        self.loginButton.loadingText = "Logining..."
    
    }
    
    // MARK: @IBAction
    
    @IBAction func clickedOnLoginButton(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if self.mailTextField.text == nil || self.mailTextField.text!.isBlank || !self.mailTextField.text!.isEmail {
            Utils.showAlertView("Please enter a valid email address", cancelHandler: { (alert) in
                self.mailTextField.becomeFirstResponder()
            })
            return
        }
        
        if self.passwordTextField.text == nil || self.passwordTextField.text!.isBlank  || (self.passwordTextField.text!.length < 6) {
            Utils.showAlertView("Password must be 6 characters, please enter password", cancelHandler: { (alert) in
                self.passwordTextField.becomeFirstResponder()
            })
            return
        }
        
        self.loginButton.loading = true
        
        let username = self.mailTextField.text!
        let password = self.passwordTextField.text!
        
        UserManager.shared.login(username: username, password: password, success: {
            self.loginButton.loading = false
            (UIApplication.shared.delegate as? AppDelegate)?.home()
        }) { (error) in
            self.loginButton.loading = false
            self.view.showError(error)
        }
    }
}


extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.mailTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.clickedOnLoginButton(self.loginButton)
        }
        return true
    }
}

