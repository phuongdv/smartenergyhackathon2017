//
//  HomeViewController.swift
//  smartmeter
//
//  Created by Mac on 9/8/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit
import ObjectMapper
import ESPullToRefresh
import Async

class HomeViewController: BaseViewController {
    
    let HCellIdentifier = "historycell"
    let ECellIdentifier = "emptycell"
    
    @IBOutlet weak var tableView: UITableView!
    var header: PowerHeader?
    var iotaLabel: UILabel?
    
    var isLoaded: Bool = false
    var isEnterBackground = false
    
    var debit: DebitAmount? {
        didSet {
            let kwh = (debit?.usd ?? 0)/0.12
            self.header?.progress = CGFloat(kwh)
            self.header?.balance = (debit?.iota ?? 0)
            self.header?.date = debit?.date
        }
    }
    
    var wallet: Wallet? {
        didSet {
            self.header?.iota = wallet?.balanceIOTA ?? 0
        }
    }
    
    var transactions: [Transaction] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.isLoaded {
            self.tableView.es_startPullToRefresh()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    deinit {
        self.removeObserver()
    }
    
    // MARK: UI
    
    func setupUI() {
        
        self.title = "NetObjex"
        self.addLogoutButtonBarItem()
        
        let nib = UINib(nibName: "HistoryTableViewCell", bundle: nil)
        let eNib = UINib(nibName: "EmptyTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: HCellIdentifier)
        self.tableView.register(eNib, forCellReuseIdentifier: ECellIdentifier)
        self.tableView.backgroundColor = UIColor.clear
        
        let height: CGFloat = UIScreen.main.bounds.width <= 320 ? 254 : 320
        let header = PowerHeader(frame: CGRect(x: 0, y: 0, w: UIScreen.main.bounds.width, h: height))
        header.backgroundColor = ColorService.backgroundColor
        self.tableView.tableHeaderView = header
        header.delegate = self
        self.header = header
        
        self.tableView.es_addPullToRefresh {
            self.load()
        }
        
        MqttClient.shared.connect()
        
        self.addObserver()
    }
    
    // MARK: Services
    
    func load() {
        
        SmartMeterService.shared.debitAmount({ (data) in
            if let debit = Mapper<DebitAmount>().map(JSON: data) {
                self.debit = debit
            }
            SmartMeterService.shared.transactions({ (data1) in
                self.transactions = []
                if let _jsonTransactions = data1["transactions"] as? [[String: Any]] {
                    for json in _jsonTransactions {
                        if let transaction = Mapper<Transaction>().map(JSON: json) {
                            if (transaction.usd ?? 0) != 0 {
                                self.transactions.append(transaction)
                            }
                        }
                    }
                    self.transactions = self.transactions.sorted(by: { (t1, t2) -> Bool in
                        return t1.date?.compare(t2.date ?? Date()) == .orderedDescending
                    })
                }
                self.tableView.es_stopPullToRefresh()
                self.isLoaded = true
                self.tableView.reloadData()
            }, failure: { (error) in
                self.tableView.es_stopPullToRefresh()
                self.view.showError(error)
            })
        }, failure: { (error) in
            self.tableView.es_stopPullToRefresh()
            self.view.showError(error)
        })
        
        SmartMeterService.shared.wallet({ (data2) in
            print(data2)
            if let wallet = Mapper<Wallet>().map(JSON: data2) {
                self.wallet = wallet
            }
        }, failure: { (error) in
            self.view.showError(error)
        })
    }
    
    // MARK: Actions
    
    override func clickedOnRightBarButtonItem() {
        Utils.showConfirmAlertView(nil, message: LOGOUT_CONFIRM_MESSAGE) { (alert) in
            UserManager.logout()
        }
    }
    
    // MARK: - Notifications
    
    func addObserver() {
        
        self.removeObserver()
        NotificationCenter.default.addObserver(self, selector: #selector(self.powerChanged(_:)),
                                               name: .powerChanged,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appDidEnterBackground(_:)),
                                       name: Notification.Name.UIApplicationDidEnterBackground,
                                       object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appDidBecomeActive(_:)),
                                               name: Notification.Name.UIApplicationDidBecomeActive,
                                               object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: .powerChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    func powerChanged(_ notification: Notification) -> Void {
        self.load()
    }
    
    func appDidEnterBackground(_ notification: Notification) -> Void {
        self.isEnterBackground = true
    }
    
    func appDidBecomeActive(_ notification: Notification) -> Void {
        if self.isEnterBackground {
            self.load()
            self.isEnterBackground = false
        }
    }
}

// MARK: - UITableView

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactions.count > 0 ? self.transactions.count : (self.isLoaded ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.transactions.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ECellIdentifier, for: indexPath) as! EmptyTableViewCell
            cell.titleLabel.text = PAYMENT_HISTORY_EMPTY
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HCellIdentifier, for: indexPath) as! HistoryTableViewCell
        cell.setTransaction(self.transactions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

// MARK: - PowerHeaderDelegate

extension HomeViewController: PowerHeaderDelegate {
    
    func didClickOnPayButton(_ header: PowerHeader, payButton: LoadingButton) {
        Utils.showConfirmAlertView(nil, message: PAYMENT_CONFIRM_MESSAGE) { (alert) in
            payButton.loading = true
            SmartMeterService.shared.pay({ (data) in
                if let status = data["status"] as? String, status == "Ok" {
                    self.load()
                    self.view.showError(PAYMENT_SUCCESS)
                } else if let message = data["message"] as? String {
                    self.view.showError(message)
                }
                payButton.loading = false
            }, failure: { (error) in
                payButton.loading = false
                self.view.showError(error)
            })
        }
    }
}
