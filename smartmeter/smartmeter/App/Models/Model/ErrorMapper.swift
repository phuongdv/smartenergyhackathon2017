//
//  ErrorMapper.swift
//  cap
//
//  Created by Mac Lich on 5/30/17.
//  Copyright © 2017 maclich. All rights reserved.
//

import UIKit
import ObjectMapper

class ErrorMapper: BaseMapper {
    
    var name: String?
    var message: String?
    var statusCode: Int?
    
    var isError: Bool {
        get {
            return message != nil && statusCode != nil
        }
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    // Mappable
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        name    <- map["error.name"]
        message    <- map["error.message"]
        statusCode    <- map["error.statusCode"]
    }
}
