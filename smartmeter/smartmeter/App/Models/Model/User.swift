//
//  User.swift
//  cap
//
//  Created by Mac Lich on 5/18/17.
//  Copyright © 2017 maclich. All rights reserved.
//

import UIKit
import ObjectMapper

class User: BaseMapper {
    
    var avatar: String?
    var token: String?
    var uid: String?
    var userUnder: String?
    var areaCode: String?
    var countryCode: String?
    var number: String?
    var email: String?
    var created: String?
    var org: String?
    var otp: Bool?
    var otpauth: Int?
    
    var password: String?
    
    var firstName: String?
    var lastName: String?
    var userName: String?
    
    var awsUserID: String?
    var awsPassword: String?
    var awsReorderConfirm: Bool?
    
    let transform = TransformOf<String, Any>(fromJSON: { (value: Any?) -> String? in
        if value is NSNumber {
            let num = (value as? NSNumber)
            return num?.stringValue
        } else if value is String {
            return value as? String
        }
        return nil
    }, toJSON: { (value: String?) -> Any? in
        return value
    })
    
    var ttl: UInt64 = 0
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    // Mappable
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        avatar    <- map["avatar"]
        token    <- map["token"]
        uid    <- map["uid"]
        userUnder    <- map["userUnder"]
        email    <- map["email"]
        created    <- map["created"]
        org    <- map["org"]
        otp    <- map["otp"]
        otpauth    <- map["otpauth"]
        areaCode    <- (map["mobile.areacode"], transform)
        countryCode    <- (map["mobile.countrycode"], transform)
        number    <- (map["mobile.number"], transform)
        password    <- map["password"]
        
        firstName    <- map["firstname"]
        lastName    <- map["lastname"]
        userName    <- map["username"]
        
        awsUserID    <- map["awsuserid"]
        awsPassword    <- map["awspassword"]
        awsReorderConfirm    <- map["awsreorderconfirm"]
        
        ttl    <- map["ttl"]
    }
}

