//
//  DebitAmount.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit
import ObjectMapper

class DebitAmount: BaseMapper {
    
    var iota: Double?
    var usd: Double?
    var date: Date?
    
    let dateTransform = TransformOf<Date, Any>(fromJSON: { (value: Any?) -> Date? in
        if value is NSNumber {
            let num = ((value as? NSNumber)?.doubleValue ?? 0)/1000
            let date = Date(timeIntervalSince1970: num)
            return date
        } else if value is String {
            let num = (Double((value as? String) ?? "0") ?? 0)/1000
            let date = Date(timeIntervalSince1970: num)
            return date
        }
        return nil
    }, toJSON: { (value: Date?) -> Any? in
        let timestamp = Int64((value ?? Date()).timeIntervalSince1970*1000)
        return timestamp
    })
    
    let transform = TransformOf<Double, Any>(fromJSON: { (value: Any?) -> Double? in
        if value is NSNumber {
            let num = (value as? NSNumber)
            return num?.doubleValue
        } else if value is String {
            return Double((value as? String) ?? "0")
        }
        return 0
    }, toJSON: { (value: Double?) -> Any? in
        return value
    })
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    // Mappable
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        iota    <- (map["iota"], transform)
        usd    <- (map["usd"], transform)
        date    <- (map["timestamp"], dateTransform)
    }
}
