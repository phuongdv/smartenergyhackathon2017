//
//  Transaction.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit
import ObjectMapper

class Transaction: BaseMapper {

    var iota: Double?
    var usd: Double?
    var date: Date?
    var transactionId: String?
    var currentIndex: Int?
    var lastIndex: Int?
    var powerConsumption: Double?
    
    let transform = TransformOf<Double, Any>(fromJSON: { (value: Any?) -> Double? in
        if value is NSNumber {
            let num = (value as? NSNumber)
            return num?.doubleValue
        } else if value is String {
            return Double((value as? String) ?? "0")
        }
        return 0
    }, toJSON: { (value: Double?) -> Any? in
        return value
    })
    
    let dateTransform = TransformOf<Date, Any>(fromJSON: { (value: Any?) -> Date? in
        if value is NSNumber {
            let num = ((value as? NSNumber)?.doubleValue ?? 0)
            let date = Date(timeIntervalSince1970: num)
            return date
        } else if value is String {
            let num = (Double((value as? String) ?? "0") ?? 0)
            let date = Date(timeIntervalSince1970: num)
            return date
        }
        return nil
    }, toJSON: { (value: Date?) -> Any? in
        let timestamp = Int64((value ?? Date()).timeIntervalSince1970)
        return timestamp
    })
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    // Mappable
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        transactionId    <- map["hash"]
        iota    <- (map["iota"], transform)
        usd    <- (map["usd"], transform)
        powerConsumption    <- (map["kwh"], transform)
        date    <- (map["timestamp"], dateTransform)
        currentIndex    <- map["currentIndex"]
        lastIndex    <- map["lastIndex"]
    }

}
