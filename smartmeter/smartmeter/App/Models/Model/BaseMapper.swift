//
//  BaseMapper.swift
//  cap
//
//  Created by Mac Lich on 5/18/17.
//  Copyright © 2017 maclich. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseMapper: Mappable {
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
    }
}
