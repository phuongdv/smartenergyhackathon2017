//
//  Wallet.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit
import ObjectMapper

class Wallet: BaseMapper {

    var balanceUSD: Double?
    var balanceIOTA: Double?
    
    let transform = TransformOf<Double, Any>(fromJSON: { (value: Any?) -> Double? in
        if value is NSNumber {
            let num = (value as? NSNumber)
            return num?.doubleValue
        } else if value is String {
            return Double((value as? String) ?? "0")
        }
        return 0
    }, toJSON: { (value: Double?) -> Any? in
        return value
    })
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    // Mappable
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        balanceUSD    <- (map["balanceUSD"], transform)
        balanceIOTA    <- (map["balanceIOTA"], transform)
    }
}
