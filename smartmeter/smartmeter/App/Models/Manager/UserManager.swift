//
//  UserManager.swift
//  clinitraq
//
//  Created by Mac Lich on 12/28/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit
import ObjectMapper

let CURRENT_USER_KEY = "netobjex_current_user_key"
let LOGIN_TIME_KEY = "netobjex_login_time_key"

typealias OptTokenSuccess = (String?) -> ()
typealias CheckLoginSuccess = (Bool) -> ()

class UserManager: NSObject {
    
    var user: User? {
        get {
            if let JSONString = UserDefaults.standard.value(forKey: CURRENT_USER_KEY) as? String {
                return Mapper<User>().map(JSONString: JSONString)
            }
            return nil
        }
        set (newVal) {
            if let JSONString = newVal?.toJSONString(prettyPrint: true)  {
                UserDefaults.standard.setValue(JSONString, forKey: CURRENT_USER_KEY)
            }
        }
    }
    
    var loggedIn: Bool {
        get {
            if let _ = UserDefaults.standard.value(forKey: CURRENT_USER_KEY) as? String {
                return true
            }
            return false
        }
    }
    
    class var shared: UserManager {
        struct Singleton {
            static let instance = UserManager()
        }
        return Singleton.instance
    }
    
    class func clear() {
        UserManager.shared.user = nil
        UserDefaults.standard.removeObject(forKey: CURRENT_USER_KEY)
        UserDefaults.standard.removeObject(forKey: LOGIN_TIME_KEY)
        UserDefaults.standard.synchronize()
    }
    
    class func logout() {
        MqttClient.logout()
        UserManager.clear()
        (UIApplication.shared.delegate as? AppDelegate)?.login()
    }
    
    func checkToken(_ success: APISuccess?, failure: APIError?) {
        let userId = self.user?.uid ?? ""
        APIService.shared.verifyToken(userId, success: { (JSON) in
            success?()
        }, failure: failure)
    }
    
    func login(username: String, password: String, success: APISuccess?, failure: APIError?) {
        APIService.shared.login(username, password: password, success: { (JSON) in
            var data = JSON
            data["password"] = password
            let user = Mapper<User>().map(JSON: data)
            if let _ = user?.uid {
                UserDefaults.standard.setValue(Date(), forKey: LOGIN_TIME_KEY)
                UserDefaults.standard.synchronize()
                self.user = user
                success?()
            } else {
                failure?(ERROR_DEFAULT)
            }
        }, failure: failure)
    }
}

