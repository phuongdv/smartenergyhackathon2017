//
//  HistoryTableViewCell.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    var attr1 = [NSFontAttributeName: FontService.robotoRegular(14)]
    var attr2 = [NSFontAttributeName: FontService.robotoLight(10)]
    
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var transLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!

    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.balanceLabel.text = "0 IOTA"
        self.powerLabel.text = "0 KwH"
        
        if UIScreen.main.bounds.width <= 320 {
            let font13 = FontService.robotoLight(12)
            self.dateTimeLabel.font = font13
            self.transLabel.font = font13
            attr1 = [NSFontAttributeName: FontService.robotoRegular(12)]
            attr2 = [NSFontAttributeName: FontService.robotoLight(8)]
            self.widthConstraint.constant = 60
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTransaction(_ transaction: Transaction) {
        
        let mmm = transaction.date?.toString(format: "MMM yyyy") ?? ""
        let dateTime = transaction.date?.toString(format: "MM/dd/yyyy hh:mm:ss a").uppercased() ?? ""
        self.dateTimeLabel.text = mmm + " (\(dateTime))"
        self.balanceLabel.attributedText = "\(Int64(transaction.iota ?? 0))".addSubFont(sub: "IOTA", seperator: " ", attr: attr1, subAttr: attr2)
        let pC = "\(transaction.powerConsumption?.currency ?? "0")"
        self.powerLabel.attributedText = pC.addSubFont(sub: "KwH", seperator: " ", attr: attr1, subAttr: attr2)
        self.transLabel.text = transaction.transactionId ?? ""
    }
}

