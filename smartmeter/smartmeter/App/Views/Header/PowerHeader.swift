//
//  PowerHeader.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit

protocol PowerHeaderDelegate {
    func didClickOnPayButton(_ header: PowerHeader, payButton: LoadingButton)
}

class PowerHeader: UIView {
    
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var progressBarView: MBCircularProgressBarView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var payButton: LoadingButton!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var rateTitleLabel: UILabel!
    @IBOutlet weak var iotaTitleLabel: UILabel!
    @IBOutlet weak var iotaLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var fromBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var balanceTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalTopConstrait: NSLayoutConstraint!
    @IBOutlet weak var rightContraints: NSLayoutConstraint!
    @IBOutlet weak var leftContraints: NSLayoutConstraint!
    @IBOutlet weak var unitConstraint: NSLayoutConstraint!
    @IBOutlet weak var updateTopConstraint: NSLayoutConstraint!
    
    var progress: CGFloat = 0 {
        didSet {
            self.progressBarView.value = progress
        }
    }
    
    var balance: Double = 0 {
        didSet {
            self.totalLabel.text = "\(Int64(balance)) IOTA"
        }
    }
    
    var iota: Double = 0 {
        didSet {
            self.iotaLabel.text = "\(Int64(iota)) IOTA"
        }
    }
    
    var date: Date? = nil {
        didSet {
            let dateTime = date?.toString(format: "MM/dd/yyyy hh:mm:ss a").uppercased() ?? ""
            self.updateLabel.text = "Last updated: \(dateTime)"
        }
    }
    
    
    var delegate: PowerHeaderDelegate?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.loadNib()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.loadNib()
    }
    
    private func loadNib() {
        
        self.view = self.loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.addSubview(self.view)
        
        self.progressBarView.decimalPlaces = 3
        self.progressBarView.progressCapType = 1
        self.progressBarView.emptyCapType = 1
        self.progressBarView.valueFontName = "Roboto-Light"
        self.progressBarView.valueFontSize = 19
        self.unitConstraint.constant = 0
        
        if UIScreen.main.bounds.width <= 320 {
            
            self.progressBarView.valueFontSize = 17
            self.progressBarView.progressLineWidth = 10
            self.progressBarView.emptyLineWidth = 10
            
            self.progressWidthConstraint.constant = 150
            self.buttonHeightConstraint.constant = 30
            self.balanceTopConstraint.constant = 6
            self.totalTopConstrait.constant = 2
            self.fromBottomConstraint.constant = -12
            self.leftContraints.constant =  8
            self.rightContraints.constant =  8
            self.updateTopConstraint.constant = 0
            
            self.unitLabel.font = FontService.robotoLight(10)
            self.fromLabel.font = FontService.robotoLight(8)
            self.updateLabel.font = FontService.robotoLight(8)
            self.powerLabel.font = FontService.robotoMedium(17)
            
            let font13 = FontService.robotoLight(12)
            let font15 = FontService.robotoRegular(12)
            self.balanceLabel.font = font15
            self.rateTitleLabel.font = font15
            self.iotaTitleLabel.font = font15
            self.totalLabel.font = font13
            self.rateLabel.font = font13
            self.iotaLabel.font = font13

            self.payButton.titleLabel?.font = FontService.robotoRegular(15)
        }
        
        self.payButton.setCornerRadius(radius: 4)
        self.payButton.addCompletionHanlder { (state) in
            
        }
        
        self.payButton.loadingText = "Submitting..."
        self.progressBarView.value = 0
        
        self.totalLabel.text = "0 IOTA"
        self.iotaLabel.text = "0 IOTA"
        self.rateLabel.text = "0.197/KwH"
    }
    
    @IBAction func clickedOnPayButton(_ sender: Any) {
        self.delegate?.didClickOnPayButton(self, payButton: self.payButton)
    }
}
