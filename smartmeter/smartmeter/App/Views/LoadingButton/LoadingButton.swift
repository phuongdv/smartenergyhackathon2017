//
//  LoadingButton.swift
//  cap
//
//  Created by Mac Lich on 5/11/17.
//  Copyright © 2017 maclich. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import EZSwiftExtensions

public enum ActivityIndicatorAlignment: Int {
    case Left
    case Right
}

public enum LoadingButtonState: Int {
    case start
    case end
}

typealias ActivityIndicatorHandler = (LoadingButtonState) -> ()

public class LoadingButton: UIButton {
    
    lazy var activityIndicatorView: NVActivityIndicatorView! = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, w: 30, h: 30), type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.white, padding: 2)
    
    public var indicatorAlignment: ActivityIndicatorAlignment = ActivityIndicatorAlignment.Right {
        didSet {
            setupPositionIndicator()
        }
    }
    
    public var activityPadding: CGFloat = 8 {
        didSet {
            setupPositionIndicator()
        }
    }
    
    public var loading: Bool = false {
        didSet {
            realoadView()
        }
    }
    
    public var indicatorColor: UIColor = UIColor.white {
        didSet {
            activityIndicatorView.color = indicatorColor
        }
    }
    
    public var normalText: String? = nil {
        didSet {
            if(normalText == nil){
                normalText = self.titleLabel?.text
            }
            
            self.titleLabel?.text = normalText
        }
    }
    
    public var loadingText: String? = ""
    
    var topContraints: NSLayoutConstraint?
    var bottomContraints: NSLayoutConstraint?
    var widthContraints: NSLayoutConstraint?
    var rightContraints: NSLayoutConstraint?
    var leftContraints: NSLayoutConstraint?
    
    var handler: ActivityIndicatorHandler?
    
    required  public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    public override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle(title, for: state)
        
        if normalText == nil{
            normalText = title
        }
    }
    
    func setupView() {
        self.normalText = self.titleLabel?.text
        self.addSubview(activityIndicatorView)
        setupPositionIndicator()
    }
    
    func realoadView() {
        if(loading){
            
            self.isEnabled = false
            activityIndicatorView.isHidden = false;
            activityIndicatorView.startAnimating()
            if(self.loadingText != nil ){
                self.setTitle(loadingText, for: .normal)
            }
            self.handler?(.start)
            
        } else{
            
            self.isEnabled = true
            activityIndicatorView.stopAnimating()
            self.setTitle(normalText, for: .normal)
            self.handler?(.end)

        }
    }
    
    func setupPositionIndicator()  {
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        if(topContraints == nil){
            topContraints = NSLayoutConstraint(item: activityIndicatorView, attribute:
                .top, relatedBy: .equal, toItem: self,
                      attribute: NSLayoutAttribute.top, multiplier: 1.0,
                      constant: 0)
        }
        
        if(bottomContraints == nil){
            bottomContraints = NSLayoutConstraint(item: activityIndicatorView, attribute:
                .bottom, relatedBy: .equal, toItem: self,
                         attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                         constant: 0)
        }
        
        if(widthContraints == nil){
            widthContraints = NSLayoutConstraint(item: activityIndicatorView, attribute:
                .width, relatedBy: .equal, toItem: nil,
                        attribute: .width, multiplier: 1.0,
                        constant: 30)
        }
        
        if(rightContraints == nil){
            rightContraints = NSLayoutConstraint(item: activityIndicatorView, attribute:
                .trailingMargin, relatedBy: .equal, toItem: self,
                                 attribute: .trailingMargin, multiplier: 1.0,
                                 constant: 0)
        }
        
        if(leftContraints == nil){
            leftContraints = NSLayoutConstraint(item: activityIndicatorView, attribute:
                .leading, relatedBy: .equal, toItem: self,
                          attribute: .leading, multiplier: 1.0,
                          constant: 0)
        }
        
        if(indicatorAlignment == .Right ){
            rightContraints?.constant = -activityPadding
            NSLayoutConstraint.deactivate([leftContraints!])
            NSLayoutConstraint.activate([topContraints!,rightContraints!,widthContraints!,bottomContraints!])
        }else{
            leftContraints?.constant = activityPadding
            NSLayoutConstraint.deactivate([rightContraints!])
            NSLayoutConstraint.activate([topContraints!,leftContraints!,widthContraints!,bottomContraints!])
        }
    }
    
    func addCompletionHanlder(completion: @escaping ActivityIndicatorHandler) {
        self.handler = completion
    }
    
    deinit {
        activityIndicatorView.removeFromSuperview()
        activityIndicatorView = nil
    }
}
