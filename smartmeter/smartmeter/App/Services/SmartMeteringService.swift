//
//  SmartMeteringService.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit
import Alamofire

class SmartMeterService {

    enum Router: URLRequestConvertible {
        
        static let baseURLString = BASE_URL_STRING_FOR_SMART_METER
        
        case debitAmount
        case transactions
        case wallet
        case pay(requestBody: Data?)
        
        public func asURLRequest() throws -> URLRequest {
            let (path, body, method): (String, Data?, Alamofire.HTTPMethod) = {
                switch self {
                case .debitAmount:
                    return ("/smartenergy/api/sender/debetAmount", nil, .get)
                case .transactions:
                    return ("/smartenergy/api/sender/transactions", nil, .get)
                case .wallet:
                    return ("/smartenergy/api/sender/wallet", nil, .get)
                case .pay(let requestBody):
                    return ("/smartenergy/api/payment", requestBody, .post)
                }
            }()
            
            let url = URL(string: Router.baseURLString + path)!
            print("URL: \(url.absoluteString)")
            var urlRequest = URLRequest(url: url)
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-type")
            urlRequest.setValue(X_APP_BUNDLE_ID, forHTTPHeaderField: "X-APP-BUNDLE-ID")
            urlRequest.setValue(X_API_AUTH_KEY, forHTTPHeaderField: "X-API-AUTH-KEY")
            if let _body = body {
                urlRequest.httpBody = _body
            }
            if let accToken = UserManager.shared.user?.token, let uid = UserManager.shared.user?.uid {
                urlRequest.setValue(accToken, forHTTPHeaderField: "X-Access-Token")
                urlRequest.setValue(uid, forHTTPHeaderField: "X-Access-UserId")
                print("User token: " + accToken + " User Id: " + uid)
            }
            urlRequest.httpMethod = method.rawValue
            return urlRequest
        }
    }
    
    class var shared: SmartMeterService {
        struct Singleton {
            static let instance = SmartMeterService()
        }
        return Singleton.instance
    }
    
    private func call(URLRequest: URLRequestConvertible, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        request(URLRequest).responseJSON(completionHandler: completionHandler)
    }
    
    private func validate(_ response: DataResponse<Any>, success: APIResult?, failure: APIError?) {
        print(response)
        if let JSON = response.result.value as? [String: Any]  {
            if let _ = JSON["error"], let message = JSON["message"] as? String {
//                let statusCode = JSON["status"] as? Int
//                if statusCode == 401 {
//                    NotificationCenter.default.post(name: .logoutNotificationName, object: nil, userInfo: nil)
//                    return
//                }
                failure?(message)
            } else {
                success?(JSON)
            }
        } else {
            failure?(ERROR_DEFAULT)
        }
    }
    
    func debitAmount(_ success: APIResult?, failure: APIError?) {
        self.call(URLRequest: Router.debitAmount) { (response) in
            self.validate(response, success: success, failure: failure)
        }
    }
    
    func transactions(_ success: APIResult?, failure: APIError?) {
        self.call(URLRequest: Router.transactions) { (response) in
            if let JSON = response.result.value as? [[String: Any]]  {
                success?(["transactions": JSON])
            } else {
                self.validate(response, success: success, failure: failure)
            }
        }
    }
    
    func wallet(_ success: APIResult?, failure: APIError?) {
        self.call(URLRequest: Router.wallet) { (response) in
            self.validate(response, success: success, failure: failure)
        }
    }
    
    func pay(_ success: APIResult?, failure: APIError?) {
        self.call(URLRequest: Router.pay(requestBody: nil)) { (response) in
            self.validate(response, success: success, failure: failure)
        }
    }
}
