//
//  MqttClient.swift
//  Netobjex
//
//  Created by Mac Lich on 6/27/17.
//  Copyright © 2017 maclich. All rights reserved.
//

import UIKit
import Moscapsule
import Async

class MqttClient: NSObject {

    var mqttClient: MQTTClient?
    
    class var shared: MqttClient {
        struct Singleton {
            static let instance = MqttClient()
        }
        return Singleton.instance
    }
    
    override init() {
        super.init()
        
        moscapsule_init()
    }
    
    func connect() {
        
        if let _ = self.mqttClient  {
            self.mqttClient = nil
        }
        
        let clientId = "ClientId_\(Date().timeIntervalSince1970)"
        let mqttConfig = MQTTConfig(clientId: clientId, host: MQTT_HOST, port: Int32(MQTT_PORT), keepAlive: 60)
        let mqttAuthOpts = MQTTAuthOpts(username: MQTT_Username, password: MATT_Password)
        mqttConfig.mqttAuthOpts = mqttAuthOpts
        mqttConfig.cleanSession = true
        mqttConfig.onConnectCallback = { returnCode in
            NSLog("Return Code is \(returnCode.description)")
            if returnCode == ReturnCode.success {
                self.subscribe()
            }
            else {
                // error handling for connection failure
            }
        }
        mqttConfig.onDisconnectCallback = { returnCode in
            self.unsubscribe()
        }
        
        mqttConfig.onMessageCallback = { mqttMessage in
            NSLog("MQTT Message received from \(mqttMessage.topic): payload=\(mqttMessage.payloadString)")
            let data = mqttMessage.payload?.jsonFromData() as? [String: Any]
            Async.main({ () -> Void in
                if mqttMessage.topic.contains("/in") {
                    NotificationCenter.default.post(name: .powerChanged, object: nil, userInfo: data)
                }
            })
        }

        self.mqttClient = MQTT.newConnection(mqttConfig)
    }
    
    func subscribe() {
        self.unsubscribe()
        self.mqttClient?.subscribe("/topic/device/smartmeter/in", qos: 2)
    }
    
    func unsubscribe() {
        self.mqttClient?.unsubscribe("/topic/device/smartmeter/in")
    }
    
    func disconnect() {
        self.unsubscribe()
        self.mqttClient?.disconnect()
    }
    
    class func logout()  {
        MqttClient.shared.disconnect()
        MqttClient.shared.mqttClient = nil
    }
}


