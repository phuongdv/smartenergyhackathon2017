//
//  FontService.swift
//  clinitraq
//
//  Created by Mac Lich on 12/26/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit

class FontService: NSObject {
    
    class func robotoRegular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size)!
    }
    
    class func robotoLight(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Light", size: size)!
    }
    
    class func robotoMedium(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size)!
    }
}
