//
//  StoryboardService.swift
//  smartmeter
//
//  Created by Mac on 9/9/17.
//  Copyright © 2017 NetObjex. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    class func mainStoryBoard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    class func rootViewController() -> UIViewController? {
        return mainStoryBoard().instantiateInitialViewController()
    }
    
    class func mainViewController() -> UIViewController? {
        return mainStoryBoard().instantiateViewController(withIdentifier: "homenavigationcontroller")
    }
}
