//
//  APIService.swift
//  clinitraq
//
//  Created by Mac Lich on 12/28/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias APIError = (String?) -> ()
typealias APIResult = ([String: Any]) -> ()
typealias APIArray = ([[String: Any]]) -> ()
typealias APISuccess = () -> ()

class APIService {
    
    enum Router: URLRequestConvertible {
        
        static let baseURLString = BASE_URL_STRING
        
        case login(requestBody: Data?)
        case verifyToken(userId: String)
        
        public func asURLRequest() throws -> URLRequest {
            let (path, body, method, accessToken): (String, Data?, Alamofire.HTTPMethod, Bool) = {
                switch self {
                
                case .login(let requestBody):
                    return ("/api/users/authenticate", requestBody, .post, false)
                case .verifyToken(let userId):
                    return ("/api/users/verifyToken?userId=\(userId)", nil, .post, true)
                }
            }()
            
            let url = URL(string: Router.baseURLString + path)!
            print("URL: \(url.absoluteString)")
            var urlRequest = URLRequest(url: url)
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-type")
            urlRequest.setValue(X_APP_BUNDLE_ID, forHTTPHeaderField: "X-APP-BUNDLE-ID")
            urlRequest.setValue(X_API_AUTH_KEY, forHTTPHeaderField: "X-API-AUTH-KEY")
            if let _body = body {
                urlRequest.httpBody = _body
            }
            if accessToken {
                if let accToken = UserManager.shared.user?.token {
                    urlRequest.setValue(accToken, forHTTPHeaderField: "X-Auth-Key")
                    print("User token: " + accToken)
                }
            }
            urlRequest.httpMethod = method.rawValue
            return urlRequest
        }

    }
    
    class var shared: APIService {
        struct Singleton {
            static let instance = APIService()
        }
        return Singleton.instance
    }
    
    private func call(URLRequest: URLRequestConvertible, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        request(URLRequest).responseJSON(completionHandler: completionHandler)
    }
    
    private func validate(_ response: DataResponse<Any>, success: APIResult?, failure: APIError?) {
        print(response)
        if let JSON = response.result.value as? [String: Any]  {
            if let _error = Mapper<ErrorMapper>().map(JSON: JSON), _error.isError {
                failure?(_error.message)
            } else {
                success?(JSON)
            }
        } else {
            failure?(ERROR_DEFAULT)
        }
    }

    // MARK: - User

    func login(_ email: String, password: String, success: APIResult?, failure: APIError?) {
        let dic: [String: String] = ["username": email, "password": password,  "remember": "true"]
        self.call(URLRequest: Router.login(requestBody: dic.dataFromJson())) { (response) in
            self.validate(response, success: success, failure: failure)
        }
    }
    
    func verifyToken(_ userId: String, success: APIResult?, failure: APIError?) {
        self.call(URLRequest: Router.verifyToken(userId: userId)) { (response) in
            self.validate(response, success: success, failure: failure)
        }
    }
}

