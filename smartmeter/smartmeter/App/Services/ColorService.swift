//
//  ColorService.swift
//  clinitraq
//
//  Created by Mac Lich on 12/26/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class ColorService: NSObject {

    static var backgroundColor: UIColor {
        get {
            return UIColor(hexString: "F7F7F7")!
        }
    }
    
    static var mainColor: UIColor {
        get {
            return UIColor(hexString: "66BFBD")!
        }
    }
    
    static var blackColor: UIColor {
        get {
            return UIColor(hexString: "#2F2F2F")!
        }
    }
}
