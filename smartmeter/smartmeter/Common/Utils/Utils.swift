//
//  Utils.swift
//  clinitraq
//
//  Created by Mac Lich on 9/5/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    class func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
    
    class func attributedPlaceholder(_ textField: UITextField, placeholder: String, color: UIColor = UIColor.lightGray, font: UIFont? = nil) {
        var attributes: [String : Any] = [NSForegroundColorAttributeName : color]
        if let _font = font {
            attributes[NSFontAttributeName] = _font
        }
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: attributes)
    }
   
    class func showAlertView(_ message: String?) {
        if message == nil {
            return
        }
        let alertView = UIAlertView(title: appName, message: message, delegate: nil, cancelButtonTitle: "Ok")
        alertView.show()
    }
    
    class func showAlertView(_ message: String?, cancelHandler:((_ alert: UIAlertView) -> Void)!) {
        if message == nil {
            return
        }
        UIAlertView.show(withTitle: appName, message: message, cancelButtonTitle: "Ok", otherButtonTitles: nil) { (alert, buttonIndex) in
            cancelHandler(alert)
        }
    }

    class func showAlertView(_ title: String? = nil, message: String?, cancelButtonTitle: String) {
        if message == nil {
            return
        }
        let alertView = UIAlertView(title: appName, message: message, delegate: nil, cancelButtonTitle: cancelButtonTitle)
        alertView.show()
    }
    
    class func showConfirmAlertView(_ title: String? = nil, message: String?, destructiveHandler:((_ alert: UIAlertView) -> Void)!) {
        if message == nil {
            return
        }
        UIAlertView.show(withTitle: appName, message: message, cancelButtonTitle: "Cancel", otherButtonTitles: ["Ok"]) { (alert, buttonIndex) in
            if buttonIndex == 1 {
                destructiveHandler(alert)
            }
        }
    }
}

