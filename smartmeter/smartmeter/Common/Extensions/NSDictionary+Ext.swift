//
//  NSDictionary+Ext.swift
//  clinitraq
//
//  Created by Mac Lich on 9/5/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import UIKit

extension NSDictionary {
    
    func stringValue(_ key: String, defaultValue: String = "") -> String {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSNumber {
                return (self.object(forKey: key) as! NSNumber).stringValue
            } else {
                return (self.object(forKey: key) as! String)
            }
        }
        return defaultValue
    }
    
    func intValue(_ key: String) -> Int {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSNumber {
                return (self.object(forKey: key) as! NSNumber).intValue
            } else {
                return (self.object(forKey: key) as! NSString).integerValue
            }
        }
        return 0
    }
    
    func int64Value(_ key: String) -> Int64 {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSNumber {
                return (self.object(forKey: key) as! NSNumber).int64Value
            } else {
                return (self.object(forKey: key) as! NSString).longLongValue
            }
        }
        return 0
    }
    
    func unsignedShortValue(_ key: String) -> UInt16 {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSNumber {
                return (self.object(forKey: key) as! NSNumber).uint16Value
            } else {
                if let result = UInt16((self.object(forKey: key) as! String)) {
                    return result
                }
            }
        }
        return 0
    }
    
    func floatValue(_ key: String) -> Float {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSNumber {
                return (self.object(forKey: key) as! NSNumber).floatValue
            } else {
                return (self.object(forKey: key) as! NSString).floatValue
            }
        }
        return 0
    }
    
    func boolValue(_ key: String) -> Bool {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSNumber {
                let value = (self.object(forKey: key) as! NSNumber).int32Value
                return value == 1 ? true : false
            } else {
                let value = (self.object(forKey: key) as! NSString).intValue
                return value == 1 ? true : false
            }
        }
        return false
    }
    
    func arrayStringValue(_ key: String) -> [String] {
        if (self.object(forKey: key) != nil) && !(self.object(forKey: key) is NSNull) {
            if self.object(forKey: key) is NSArray {
                if let arrayString = self.object(forKey: key) as? [String] {
                    return arrayString
                }
            }
        }
        return []
    }
    
    func encodeToString() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            let result = String(data: jsonData, encoding: String.Encoding.utf8)
            return result
        } catch let error as NSError {
            print(error)
            return nil
        }
    }
    
    class func decodeToNSDictionary(_ encodeStr: String?) -> NSDictionary? {
        if let data = encodeStr?.data(using: String.Encoding.utf8) {
            do {
                let decoded = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                return decoded
            } catch let error as NSError {
                print(error)
                return nil
            }
        }
        return nil
    }
}
