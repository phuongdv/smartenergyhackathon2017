//
//  UILabel+MLExt.swift
//  clinitraq
//
//  Created by Mac Lich on 8/28/16.
//  Copyright © 2016 maclich. All rights reserved.
//

import Foundation
import UIKit
import Toast

extension UILabel {
    
    class func requiredHeight(_ text: String, font: UIFont, width: CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}

extension NSAttributedString {
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return boundingBox.height
    }
}

extension String {
    func removeSpecialCharsFromString(_ allowChars: Set<Character>) -> String {
        return String(self.characters.filter {allowChars.contains($0) })
    }
    
    func jsonFromString() -> [String: Any] {
        if let data = self.data(using: .utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    return json
                }
            } catch {
                
            }
        }
        return [:]
    }
    
    func addSubFont(sub: String, seperator: String, attr: [String: Any], subAttr: [String: Any]) -> NSMutableAttributedString {
        let string = "\(self)\(seperator)\(sub)"
        let _attr = NSMutableAttributedString(string: string, attributes: attr)
        let range = NSRange(location: self.length, length: (string.length - self.length))
        _attr.addAttributes(subAttr, range: range)
        return _attr
    }
}

extension UIImageView {
    
    func colorForImage(_ image: UIImage?, tintColor: UIColor)  {
        self.image = image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = tintColor
    }
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}

extension UIView {
    
    public func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func addShadow(_ radius: CGFloat = 4, opacity: Float = 0.03, size: CGSize = CGSize(width: 0, height: 2.0)) {
        
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = size
        self.layer.shadowRadius = radius
    }
    
    func showError(_ message: String?) {
        
        let style = CSToastStyle(defaultStyle: ())
        style?.messageFont = UIFont(name: "Roboto-Regular", size: 15)
        style?.messageColor = UIColor.white
        self.makeToast(message, duration: 3.0, position: CSToastPositionBottom, style: style)
    }
}

extension Double {
    
    var currency: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        let dNumber = NSNumber(value: self)
        return (formatter.string(from: dNumber) ?? "\(self)")
    }
}


extension Dictionary {
    
    public func dataFromJson() -> Data? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions()) {
            return jsonData
        }
        return nil
    }
    
    public func stringFromJson() -> String? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions()) {
            return String(data: jsonData, encoding: .utf8)
        }
        return nil
    }
}

extension Array {
    
    public func dataFromJson() -> Data? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions()) {
            return jsonData
        }
        return nil
    }
}

extension Data {
    
    public func jsonFromData() -> AnyObject? {
        if let jsonData = try? JSONSerialization.jsonObject(with: self, options: .mutableContainers) {
            return jsonData as AnyObject?
        }
        return nil
    }
}
